#!/usr/bin/env python

from __future__ import print_function
import getpass

if __name__ == "__main__":
    pswd = getpass.getpass('Vault Password:')
    print(pswd)
