#!/bin/bash

# Install virtualenv with pip if not already installed.
# pip must be installed before executing this script

if [[ $# -ne 1 ]] && [[ $# -ne 2 ]]; then
    echo "Wrong number of arguments. Usage: $0 <PYTHON_INTERPRETER> [<PIP_EXTRA_ARGS>]"
    exit 1
fi

PIP_COMMAND="$1 -m pip"

PIP_EXTRA_ARGS="$2"

"$($PIP_COMMAND freeze | grep virtualenv > /dev/null  2>&1)"

is_installed=$?

if [ "$is_installed" -eq 0 ]; then
  echo 'virtualenv is already installed'
  exit 0

else
  "$($PIP_COMMAND install virtualenv $PIP_EXTRA_ARGS > /dev/null  2>&1)"
  echo 'virtualenv has been installed'
  exit 0
fi
