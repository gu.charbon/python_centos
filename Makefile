.DEFAULT_GOAL := prepare_demo

VERSION	:= 1.0
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
CUR_DATE := $(shell date +"%s")

PLAYBOOK_FOLDER := playbooks

INVENTORY_FOLDER := "$(PLAYBOOK_FOLDER)/inventories"

TARGET_INVENTORY := test_centos.yml

TARGET_INVENTORY_PATH := "$(INVENTORY_FOLDER)/$(TARGET_INVENTORY)"


VAULT_ID := --vault-id @prompt


install_rh-python36:
	ansible-playbook -i "$(TARGET_INVENTORY_PATH)" \
	  $(VAULT_ID) \
	  "$(PLAYBOOK_FOLDER)/install_rh-python.yml"

prepare_rh-python36_centos:
	ansible-playbook -i "$(TARGET_INVENTORY_PATH)" \
	  $(VAULT_ID) \
	  "$(PLAYBOOK_FOLDER)/prepare_rh-python_centos.yml"

install_pip:
	ansible-playbook -i "$(TARGET_INVENTORY_PATH)" \
	  $(VAULT_ID) \
	  "$(PLAYBOOK_FOLDER)/install_pip.yml"

prepare_python36_epel:
	ansible-playbook -i "$(TARGET_INVENTORY_PATH)" \
	  $(VAULT_ID) \
	  "$(PLAYBOOK_FOLDER)/prepare_python_epel.yml"

docker_requirements:
	docker build -f docker/Dockerfile_requirements -t python36_demo:latest .

docker_package:
	docker build -f docker/Dockerfile_package -t python36_demo_package:latest .
