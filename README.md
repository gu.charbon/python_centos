# Setup your Python environment with Ansible

Table of Content:

* [Getting started](#getting_started)
  * [Available ansible roles](#ansible_roles)
  * [Available ansible playbooks](#ansible_playbooks)
    * [install_pip.yml](#install_pip)
    * [prepare_rh-python36](#demo_nodes)
    * [prepare_rh-python36_centos](#centos_nodes)
* [Authentication](#authentication)
  * [How to generate new value for `ansible_become_pass` variable](#ansible_become_pass)
  * [Providing Ansible vault secrets](#vault_secrets)


## Getting started
<a name="getting_started"></a>

This project uses Ansible to configure remote or local python environment.
You must use `ansible>=2.6`.

You can install it directly with `pip install -r requirements.txt` after cloning the repository.


#### Available ansible roles
<a name="ansible_roles"></a>

This repository is composed of 3 [ansible roles](https://docs.ansible.com/ansible/2.6/user_guide/playbooks_reuse_roles.html):

- **get-pip**: Can be used to install pip and virtualenv
-
- **pip**: Can be use to perform operations with pip
-
- **yum**: Can be used to perform action with yum


#### Available ansible playbooks
<a name="ansible_playbooks"></a>

Three [major playbooks](https://docs.ansible.com/ansible/devel/user_guide/playbooks.html) are available for direct usage:

> Note: You must be inside the playbooks directory to run example commands. 


- _**install_pip.yml**_:
<a name="install_pip"></a>

This playbook install `pip` and `virtualenv` with `ansible_python_interpreter` by default. Any other interpreter can be used if `python_interpreter` variable is defined.

###### Workflow:

1) Check if pip is installed
  If it is not instaled:
  1.1) Download get-pip installer
  1.2) Execute get-pip installer
  1.3) Remove get-pip installer
2) Install virtualenv if it is not installed yet

###### Example usage:

- `python_interpreter` is not defined in inventory neither in any variable.
  Default interpreter (`ansible_python_interpreter`) is used:

  ```
  ansible-playbook -i inventories/test_centos.yml \
    --vault-id @prompt \
    install_pip.yml
  ```

- `python_interpreter` is set to `/opt/rh/rh-python36/root/bin/python`

  ```
  ansible-playbook -i inventories/test_centos.yml \
    --vault-id @prompt \
    --extra-vars "python_interpreter=/opt/rh/rh-python36/root/bin/python"
    install_pip.yml
  ```


- _**prepare_rh-python36.yml**_
<a name="demo_nodes"></a>


This playbook assume that rhel repositories has already been enabled and `rh-pyhton36` package is available for download. It installs `rh-python36` and dependencies based on requirements file.

> Note: The playbook installs `rh-python36` because `ansible_python_interpreter` is set to `rh_python_interpreter` and `rh_python_interpreter` is set to `rh-python36` in group demo_nodes in inventory. You can change it to anyother package.

> Warning: If you want to install a package non managed by scl, set `scn_enable_python` to `false`.

###### Workflow:

1) Ensure `rh-pyhton-36` is installed
2) Optionally add line in .bashrc to enable rh-python36 with scl at startup
3) Copy python requirements to rmeote host
4) Install python requirements with pip from `rh-python36`

###### Example usages:

- `rh-python-interpreter` is set to `rh-python36` by default

  ```
  ansible-playbook -i inventories/test_centos.yml \
    --vault-id @prompt \
    prepare_rh-python36.yml
  ```

- Install another version of Python:

```
ansible-playbook -i inventories/test_centos.yml \
  --ask-vault-pass \
  --extra-vars "rh_python_package=rh-python35 python_interpreter=/opt/rh/rh-python35/root/bin/python" \
  prepare_rh-python36.yml
```

- Install with another requirements file:
```
ansible-playbook -i inventories/test_centos.yml \
  --vault-id @prompt \
  --extra-vars "pip_requirements_path=~/some_folder/requirements.txt"
  prepare_rh-python36.yml
```

- _**prepare_rh-python36_centos.yml**_:
<a name="centos_nodes"></a>

This playbook enables `centos-sclo-rh-testing` repository and
download `rh-python36` before installing python dependencies with this
interpreter.

###### Playbook workflow:

1) Ensure centos-release-scl is installed
2) Enable centos-sclo-rh-testing repository
3) Install `rh-python36`
2) Add line to `.bashrc` to enable rh-python36 by default at startup
3) Copy python requirements to remote host
4) Install python requirements

It ban be used the same wah `prepare_rh-python36` is used.


## Authentication
<a name="authentification"></a>

- Logging to remote host is realized with SSH Key-Based Authentication.

> Use `ssh-copy-id <ansible_user@ansible_remote_host>`  to ensure your own ssh key is authorized by remote agent.

- [Privilege escalation method](https://docs.ansible.com/ansible/latest/user_guide/become.html) used in playbook can be configured with `ansible_become_method` variable.
  Default value is `su`. Password of root user is expected to be present as vault encrypted variable named `ansible_become_pass`. Is can be accessed as any other vault secrets once the vault password is given to playbooks.

> List of available values: https://docs.ansible.com/ansible/latest/user_guide/become.html#command-line-options

## How to generate new value for `ansible_become_pass` variable ?
<a name="ansible_become_pass"></a>

- Run the following command

  ```
  ansible-vault encrypt_string "PASSWORD"
  ```

- Before returning the encrypted string it will ask you for a pasword (you will provide this password at runtime to decrypt secret). Store it into a file like the following:

You shoud get something like:

```
ansible_become_pass: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          34306464383862303338666336306239306335393366656136313362643334383264326530333136
          3831326639343639643063643664666331356236346239640a346531326465333330363761373831
          61353139323635333461313732386538366361326163613865333462353161623039356433643032
          3962303266363532330a616432653534333431363938386531373864616635393462356337336334
          3834
```

> See [official documentation](https://docs.ansible.com/ansible/2.4/vault.html#use-encrypt-string-to-create-encrypted-variables-to-embed-in-yaml) for more information.


## Providing vault secrets
<a name="vault_secrets"></a>

You can choose several options to [provide vault password](https://docs.ansible.com/ansible/2.4/vault.html#providing-vault-passwords) to playbooks at runtime:

- Using `--vault-id @prompt`.

  Example:

  ```
  ansible-playbook -i inventories/test_centos.yml \
    --vault-id @prompt \
    prepare_rh-python36.yml
  ```

- Using a file or an executable :

  Examples:

  - Assuming `get_vault_id.py` is an existing python script:
    ```
    ansible-playbook -i inventories/test_centos.yml \
      --vault-id get-vault-password.py \
      prepare_rh-python36.yml
    ```

  - Assuming `.vaultpassword` is an existing file:
  ```
  ansible-playbook -i inventories/test_centos.yml \
    --vault-id .vaultpassword \
    prepare_rh-python36.yml
  ```
